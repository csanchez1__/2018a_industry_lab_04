package ictgradschool.industry.lab04.ex05;

public class Pattern {

        int number;
        char character;
        public Pattern(int a, char c){
            number = a;
            character = c;
        }
        public void setNumberOfCharacters(int numChar){
            number = numChar;
        }
        public int getNumberOfCharacters() {
            return number;
        }
        public String toString(){
            String output = new String();
            for(int i = 0; i<number; i++){
                output = output + character;
            }
            return output;
        }
}
