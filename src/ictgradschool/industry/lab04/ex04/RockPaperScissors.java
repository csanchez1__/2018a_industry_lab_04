package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        String name = new String();
        int choice = 0;
        boolean result;
        System.out.print("Hi! What is your name? ");
        name = Keyboard.readInput();
        while (choice != QUIT){
            int computerChoice = (int)((Math.random()) * ((3-1)+1)+1);
            System.out.print("1. Rock\n" + "2. Scissors\n" + "3. Paper\n" + "4. Quit\n" + "Enter choice: ");
            choice = Integer.parseInt(Keyboard.readInput());
            if (choice == QUIT) {
                System.out.println("Goodbye " + name + ". Thanks for playing :)");
                break;
            }
            displayPlayerChoice(name, choice);
            result = userWins(choice, computerChoice);
            if (computerChoice == 1){
                System.out.println("Computer chose rock");
            }else if (computerChoice == 2) {
                System.out.println("Computer chose scissors");
            }else if (computerChoice == 3) {
                System.out.println("Computer chose paper");
            }
            if (result == true){
                System.out.println(name + " wins because " + getResultString(choice, computerChoice));
            }else if (choice == computerChoice){
             System.out.println("No one wins. " + getResultString(choice, computerChoice));
            }else if (result == false){
                System.out.println("The computer wins because " + getResultString(choice, computerChoice));
            }

        }
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        if (choice == ROCK){
            System.out.println(name + " chose rock.");
        }else if (choice == SCISSORS){
            System.out.println(name + " chose scissors.");
        }else if (choice == PAPER) {
            System.out.println(name + " chose paper.");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == 1 && computerChoice == 2){
            return true;
        } else if (playerChoice == 2 && computerChoice == 3){
            return true;
        }else  if (playerChoice == 3 && computerChoice == 1){
            return true;
        }
        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == 3 && computerChoice == 1 || computerChoice == 3 && playerChoice == 1){
            return  PAPER_WINS;
        }else if (playerChoice == 1 && computerChoice == 2 || computerChoice == 1 && playerChoice == 2){
            return ROCK_WINS;
        }else if (playerChoice == 2 && computerChoice == 3 || computerChoice == 2 && playerChoice == 3){
            return SCISSORS_WINS;
        } else if (playerChoice == computerChoice){return TIE;}
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
